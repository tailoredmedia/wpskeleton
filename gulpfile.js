// Set up constants & dependencies
const gulp = require('gulp');
const sass = require('gulp-dart-sass');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const mergeStream = require('merge-stream');
const minify = require('gulp-minify');



// Define your theme path here for task running.
// TODO: Change this when you name your theme folder.
var paths = {
    theme_src: 'wp-content/themes/tailored-starter-genesis/src/',
    theme_dst: 'wp-content/themes/tailored-starter-genesis/',
}


// Compress images
function theme_images() {
    return gulp.src(paths.theme_src + 'images/*')
        .pipe(imagemin([
            imagemin.mozjpeg({ quality: 75, progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
        ]))
        .pipe(gulp.dest(paths.theme_dst + 'images'));
}


// CSS & SASS files
function theme_styles() {
    return mergeStream(
        gulp.src(paths.theme_src + 'sass/style.scss')
            .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
            .pipe(gulp.dest(paths.theme_dst))
            .pipe(concat('style.min.css'))
            .pipe(cleanCSS())
            .pipe(gulp.dest(paths.theme_dst + 'css')),
        gulp.src(paths.theme_src + 'sass/editor.scss')
            .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
            .pipe(concat('editor.min.css'))
            .pipe(cleanCSS())
            .pipe(gulp.dest(paths.theme_dst + 'css'))
    );
}
exports.theme_styles = theme_styles;


// Theme Javascript
function theme_js() {
    minify_opts = {
        //noSource: true,
        preserveComments: 'some',
        ext: {
            min: '.min.js'
        }
    };
    return mergeStream(
        gulp.src([
                paths.theme_src + 'js/plugins.js',
                paths.theme_src + 'js/custom.js'
            ])
            .pipe(concat('custom.js'))
            .pipe(minify(minify_opts))
            .pipe(gulp.dest(paths.theme_dst + 'js')),
        gulp.src(paths.theme_src + 'js/gutenberg-admin.js')
            .pipe(minify(minify_opts))
            .pipe(gulp.dest(paths.theme_dst + 'js'))
    );
}
exports.theme_js = theme_js;


// Run all theme tasks
exports.theme = gulp.parallel(theme_images, theme_styles, theme_js);


// Watch task
function watch() {
    gulp.parallel(theme_images, theme_styles, theme_js);
    gulp.watch(paths.theme_src + 'images/*', theme_images);
    gulp.watch(paths.theme_src + 'sass/**/*', theme_styles);
    gulp.watch(paths.theme_src + 'js/**/*.js', theme_js);
}
exports.watch = watch;

