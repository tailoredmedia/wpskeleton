<?php
/**
 *  Local config file, not for production
 */

// Site URL.
define( 'WP_HOME', 'http://main/DOMAIN.com.au' );
define( 'WP_SITEURL', WP_HOME . '/wordpress' );

// Database.
define( 'DB_NAME', 'TODO' );
define( 'DB_USER', 'TODO' );
define( 'DB_PASSWORD', 'TODO' );

// Debug.
define( 'WP_DEBUG', true );
